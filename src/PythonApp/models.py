from __future__ import unicode_literals #New line in Django1.9

from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils import timezone

def my_file_uploader(instance,filename):
	return "%s/%s"%(instance.id,filename)



class PostManager(models.Manager):
	def all(self, *args, **kwargs):
		return super(PostManager, self).filter(draft=False).filter(published__lte=timezone.now())

class Post(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL)
	title = models.CharField(max_length=120)
	content = models.TextField()
	# image = models.FileField(null=True, blank=True)
	slug = models.SlugField(unique=True)
	image = models.ImageField(null=True,blank=True, 
				height_field="height_field", 	#Only with ImageField
				width_field="width_field", 		#Only with ImageField
				upload_to=my_file_uploader)
	height_field = models.IntegerField(default=0) #Value will be automatically set based on uploaded images
	width_field = models.IntegerField(default=0) #Value will be automatically set based on uploaded images
	draft = models.BooleanField(default=False)
	published = models.DateField(auto_now_add=False, auto_now=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now_add=False, auto_now=True)

	objects = PostManager()

	def __unicode__(self):
		return self.title

	def get_absolute_url(self):
		return reverse("posts:detail",kwargs={"id":self.id})
		# return "/posts/%s"%(self.id)

	class Meta:
		ordering = ["-timestamp","-updated"]