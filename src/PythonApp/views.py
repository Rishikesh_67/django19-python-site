from django.http import HttpResponse, HttpResponseRedirect,Http404
from django.shortcuts import render, get_object_or_404,redirect
from .models import Post
from .forms import PostForm
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.signals import pre_save
from django.utils.text import slugify
from urllib import quote_plus
from django.utils import timezone
from django.db.models import Q 

def home(request):
	return render(request,"home.html",{})
	
def post_list(request):
	queryset_list = Post.objects.all()
	# queryset_list = Post.objects.filter(draft=False).filter(published__lte=timezone.now())
	
	query = request.GET.get("q")
	if query:
		# queryset_list = Post.objects.filter(title__icontains=query)
		queryset_list = Post.objects.filter(
			Q(title__icontains=query)|
			Q(content__icontains=query)|
			Q(user__first_name__icontains=query)|
			Q(user__last_name__icontains=query)
			)

	paginator = Paginator(queryset_list, 5) # Show 5 contacts per page
	paginator_var = "posts-page"
	page = request.GET.get(paginator_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)

	context = {
		"title" : "Context List",
		"posts":queryset,
		"paginator_var" : paginator_var
	}
	# if request.user.is_authenticated():
	# 	context = {
	# 		"title" : "Context List(authenticated user)"
	# 	}
	# else:
	# 	context = {
	# 	"title" : "Context List(non-authenticated user)"
	# 	}
	return render(request,"post_list.html",context)
	# return HttpResponse("<h1>List - The great Python!!!</h1>")

def post_create(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404()
	form = PostForm(request.POST or None,request.FILES or None)
	# print form.cleaned_data
	# print form.cleaned_data.get("title")
	if form.is_valid():
		print form.cleaned_data.get("title")
		instance = form.save(commit=False)
		instance.user = request.user #Added by me(As I searched but didn't get, finally it worked)
		instance.save()
		messages.success(request,"Post successfully created",extra_tags="html_success")
		# print "***",list(messages.get_messages(request)),"***"
		# messages.success(request,"Thanks for your post,")
		# print "....",list(messages.get_messages(request)),"..."
		return HttpResponseRedirect(instance.get_absolute_url())
	#else:
		#messages.error(request,"Error while creating Post")
		#print type(messages)
		print "***",list(messages.get_messages(request)),"***"
		print "Form is not valid."

	context = {
		"form" : form,
		"title" : "Create Form",
		"value" : "Create post"
	}
	return render(request,"post_form.html",context)
	#return HttpResponse("<h1>Create - The great Python!!!</h1>")

def post_update(request,id=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404()
	instance = get_object_or_404(Post,id=id)
	form = PostForm(request.POST or None,request.FILES or None, instance=instance)

	if form.is_valid():
		print form.cleaned_data.get("title")
		instance = form.save(commit=False)
		instance.save()
		messages.success(request,"Post successfully updated,<a href='#'>Nice</a>",extra_tags="html_safe")
		return HttpResponseRedirect(instance.get_absolute_url())

	context = {
		"title" : instance.title,
		"form" : form,
		"value" : "Update post"
	}
	return render(request,"post_form.html",context)
	# return HttpResponse("<h1>Update - The great Python!!!</h1>")

def post_detail(request,id=None):
	# instance = Post.objects.get(id=4)
	instance = get_object_or_404(Post,id=id)
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	share_string = quote_plus(instance.content)
	context = {
		"title" : instance.title,
		"instance" : instance,
		"share_string" : share_string,
	}
	return render(request,"post_detail.html",context)
	# return HttpResponse("<h1>Detail - The great Python!!!</h1>")

def post_delete(request,id=None):
	instance = get_object_or_404(Post,id=id)
	if instance.draft or instance.published > timezone.now().date(): #Future posts
		if not request.user.is_staff or not request.user.is_superuser:
			raise Http404()
	
	instance.delete()
	messages.success(request, "POST successfully deleted")
	return redirect("posts:list")
	# return HttpResponse("<h1>Delete - The great Python!!!</h1>")

#Slugs, pre_save
def create_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if new_slug is not None:
		slug = new_slug
	qs = Post.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s"%(slug,qs.first().id)
		return create_slug(instance,new_slug)
	return slug

def pre_save_post_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)
	

pre_save.connect(pre_save_post_receiver,sender=Post)


