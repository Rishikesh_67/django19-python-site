# django19-pytohn-site	
=======================



A project containing the frontend & backend resources for a website where people can post, like, comment, share and perform other more activities. 

|   Technology used   |	Version	|
|   ----------------- | ------- |
|   Django            | 1.9     |
|   Python            | 2.7.12  |
|	HTML 			  | 5		|
|	Bootstrap		  | 3.3.7	|

